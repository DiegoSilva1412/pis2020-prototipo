import React, {Component} from 'react';
import { withRouter } from 'react-router-dom';
import axios from 'axios';

import AppFrame from './../components/AppFrame';


class HomeContainer extends Component {
    constructor(props) {
        super(props);

        this.state = {
            message: "Controller not found"
        };    
    }
      
    componentDidMount() {
        axios.get('http://localhost:3000/public/ping', 
                 {headers: {"Access-Control-Allow-Origin": "*"} 
        }).then((res) => {  
            this.setState({message : res.data.msg});
        }).catch(function(error) {
            console.log(error.message)
        });

    }

    render(){

        return (
            <div>
                <AppFrame
                    header="Welcome to the web questionnaire portal."
                    body={
                        <div>
                        <p><strong>Message from Backend API:      </strong>{this.state.message}</p>
                     </div>
                    }
                >
                </AppFrame>   
            </div>
        );
    }
}


export default withRouter(HomeContainer);

