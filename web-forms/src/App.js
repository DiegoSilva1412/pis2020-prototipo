import React, { Component } from 'react';
import {BrowserRouter as Router, Route, Switch} from 'react-router-dom';
import HomeContainer from './containers/HomeContainer';
import SimpleAppBar from './utils/SimpleAppBar'
import './App.css';

function App() {
  return (
    <div>
      <SimpleAppBar title={"December Labs"}></SimpleAppBar>
      <Router>
      <div>
        <Route exact path="/" component={HomeContainer}/>
      </div>
    </Router>
    </div>

  );
}

export default App;
