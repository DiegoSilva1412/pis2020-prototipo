import React from 'react';
import AppHeader from './AppHeader'

const AppFrame = ({header, body, footer}) => {
    return (
        <div>
            <div className="app-frame">
                <AppHeader title={header}></AppHeader>
                <div>{body}</div>
            </div>  
        </div>
    );
};

export default AppFrame;
