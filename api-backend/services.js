var express = require("express");
var app = express();
var cors = require('cors');
app.use(cors());
app.listen(3000, () => {
 console.log("Server running on port 3000");
});

app.get("/public/ping", (req, res, next) => {
    res.json({'msg' : "¡¡¡Node controller at your service!!!"});
});